import os
import numpy as np

script_dir = os.path.dirname(__file__)
with open(script_dir+"/input.txt") as f:
    depths = [int(i) for i in f.read().splitlines()]

num_inc = np.sum(np.diff(depths) > 0)
print("P1: {num_inc}".format(num_inc=num_inc))

window_diff = [depths[i + 3] - depths[i] for i in range(int(len(depths) -3))]
window_inc = np.sum(np.array(window_diff) > 0)
print("P2: {window_inc}".format(window_inc=window_inc))
