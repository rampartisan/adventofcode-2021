import os
import copy
import numpy as np


class Board:
    def __init__(self, board_mat: np.array):
        self.mat = np.ma.array(board_mat, mask=np.zeros_like(board_mat))

    def check_number(self, num: int):
        self.mat.mask[np.where(self.mat.data == num)] = 1

    def check_bingo(self) -> bool:
        if np.any(np.sum(self.mat.mask, axis=1) == 5):
            return True
        if np.any(np.sum(self.mat.mask, axis=0) == 5):
            return True
        return False

    def sum_unmarked(self):
        return self.mat.sum().sum()


def play_squid_game(numbers, boards, return_first) -> int:
    last_winning_score = 0
    boardz = copy.deepcopy(boards)
    for n in numbers:
        for b in boardz[:]:
            b.check_number(n)
            if b.check_bingo():
                last_winning_score = b.sum_unmarked() * n
                boardz.remove(b)
                if return_first:
                    return last_winning_score
    return last_winning_score


board_dim = (5, 5)
script_dir = os.path.dirname(__file__)
with open(script_dir+"/input.txt") as f:
    numbers = [int(x) for x in f.readline().strip().split(',')]
    raw_boards = [list(map(int, list(filter(None, i.split(' ')))))
                  for i in filter(None, f.read().splitlines())]
    raw_boards = np.array(raw_boards).reshape(-1, board_dim[0], board_dim[1])
    boards = [Board(x) for x in raw_boards]

print("P1: {num}".format(num=play_squid_game(numbers, boards, True)))
print("P2: {num}".format(num=play_squid_game(numbers, boards, False)))
