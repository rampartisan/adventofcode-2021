import itertools
import os
import numpy as np

script_dir = os.path.dirname(__file__)
with open(script_dir+"/input.txt") as f:
    mat = np.array([list(map(int,list(i))) for i in f.read().splitlines()])

byte_size = len(mat[0,:])

gamma =[np.argmax(np.bincount(mat[:,i])) for i in range(byte_size)]
epsilon =  [x ^ 1 for x in gamma]

print("P1: {num}".format(num=int(''.join(map(str,gamma)),2)*int(''.join(map(str,epsilon)),2)))

oxy = c02 = mat

for i in range(len(oxy[0,:])):
    if len(oxy) > 1:
        mcv_oxy = int(sum(oxy[:,i] == 1) >= len(oxy)/2)
        oxy = oxy[np.where(oxy[:,i] == mcv_oxy)]
    if len(c02) > 1:
        lcv_c02 = int(sum(c02[:,i] == 1) < len(c02)/2)
        c02 = c02[np.where(c02[:,i] == lcv_c02)]

print("P2: {num}".format(num=int(''.join(map(str,oxy.flatten())),2)*int(''.join(map(str,c02.flatten())),2)))
