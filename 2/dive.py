import os

script_dir = os.path.dirname(__file__)
depth = horizontal_pos = depth_aim = aim = 0
with open(script_dir+"/input.txt") as f:
    for i in f.read().splitlines():
        instr, val = i.split(' ')
        val = int(val)
        if instr == 'forward':
            horizontal_pos += val
            depth_aim += val * depth
        if instr == 'down':
            depth += val
        if instr == 'up':
            depth -= val

print("P1: {pos}".format(pos=depth*horizontal_pos))
print("P2: {pos}".format(pos=depth_aim*horizontal_pos))
