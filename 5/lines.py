import os
import re
from collections import namedtuple
from collections import Counter
import math

Point = namedtuple('Point', ['x', 'y'])
Line = namedtuple('Line', ['start', 'end'])

in_re = re.compile('([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)')
lines = []
with open(os.path.dirname(__file__)+"/input.txt") as f:
    for l in f.read().splitlines():
        match = in_re.match(l)
        lines.append(Line(Point(int(match.group(1)), int(match.group(2))),
                     Point(int(match.group(3)), int(match.group(4)))))


def is_horizontal_or_vertical(line: Line):
    if line.start.x == line.end.x or line.start.y == line.end.y:
        return True
    else:
        return False


def get_point_count_for_lines(lines):
    points = Counter()
    unique_line_points = set()

    for l in lines:
        unique_line_points.clear()
        x_current = l.start.x
        y_current = l.start.y
        dx = l.end.x - l.start.x
        dy = l.end.y - l.start.y

        length = math.sqrt(math.pow(dx, 2) +
                           math.pow(dy, 2))
        x_step = dx/length
        y_step = dy/length

        for _ in range(math.ceil(length) + 1):
            if x_step >= 0:
                x_quant = math.floor(x_current)
            else:
                x_quant = math.ceil(x_current)

            if y_step >= 0:
                y_quant = math.floor(y_current)
            else:
                y_quant = math.ceil(y_current)
            unique_line_points.add((x_quant, y_quant))
            x_current += x_step
            y_current += y_step
        points.update(unique_line_points)

    return points


lines_hv = list(filter(is_horizontal_or_vertical, lines))
hv_points = get_point_count_for_lines(lines_hv)
num_intersect_hv = len([i for i in hv_points.values() if i > 1])
print("P1: {}".format(num_intersect_hv))

diag_points = get_point_count_for_lines(lines)
num_intersect_diag = len([i for i in diag_points.values() if i > 1])
print("P2: {}".format(num_intersect_diag))
