import os
import numpy as np

timers = np.zeros(9, np.int64)
with open(os.path.dirname(__file__)+"/input.txt") as f:
    for t in [int(i) for i in f.readline().split(',')]:
        timers[t] += 1

def go_fish(timers, days):
    for _ in range(days):
        timers = np.roll(timers, -1)
        timers[6] += timers[8]
    return timers
    
print("P1: {}".format(np.sum(go_fish(timers,80))))
print("P2: {}".format(np.sum(go_fish(timers,256))))
